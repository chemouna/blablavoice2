(ns blablavoice2.test-runner
  (:require
   [doo.runner :refer-macros [doo-tests]]
   [blablavoice2.core-test]))

(enable-console-print!)

(doo-tests 'blablavoice2.core-test)
